package se.veritate.bokio.client

actual fun getEnv(variable: String): String = System.getenv(variable) ?: error("Missing environment variable $variable")