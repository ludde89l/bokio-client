package se.veritate.bokio.client

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.cookies.HttpCookies
import io.ktor.client.plugins.logging.DEFAULT
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json

actual fun createDefaultClient(config: HttpClientConfig<*>.() -> Unit): HttpClient =
   HttpClient(CIO) {
      install(ContentNegotiation) {
         json(
            Json {
               ignoreUnknownKeys = true
               encodeDefaults = true
            },
         )
      }

      config()

      install(Logging) {
         logger = Logger.DEFAULT
         level = LogLevel.ALL
      }

      install(HttpCookies)
   }
