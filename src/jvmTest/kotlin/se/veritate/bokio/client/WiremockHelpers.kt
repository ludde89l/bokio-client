package se.veritate.bokio.client

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.MappingBuilder
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.stubbing.StubMapping
import com.github.tomakehurst.wiremock.verification.LoggedRequest
import org.intellij.lang.annotations.Language

fun WireMockServer.forAnyRequest(responseDefinitionBuilder: ResponseDefinitionBuilder): StubMapping =
   stubFor(WireMock.any(WireMock.anyUrl()).willReturn(responseDefinitionBuilder))

/**
 * Can be used to apply multiple stubs in a consecutive block:
 * ```kotlin
 * server.se.veritate.bokio.client.stubbing(
 *   get("/v1/user").willReturn(ok()),
 *   post("/v1/user").willReturn(ok())
 * )
 * ```
 */
fun WireMockServer.stubbing(vararg mappings: MappingBuilder) {
   mappings.forEach { stubFor(it) }
}

val WireMockServer.allRequests: List<LoggedRequest>
   get() = findAll(WireMock.anyRequestedFor(WireMock.anyUrl()))

infix fun MappingBuilder.returnsJson(@Language("json") response: String): MappingBuilder =
   this.willReturn(WireMock.okJson(response))

fun wireMockServer(configuration: WireMockConfiguration.() -> Unit) =
   WireMockServer(WireMockConfiguration().apply(configuration))

infix fun MappingBuilder.returnsJsonResource(@Language("path") resourcePath: String): MappingBuilder =
   this returnsJson readResource(resourcePath)

fun readResource(path: String): String =
   object {}.javaClass.getResource(path)?.readText() ?: error("Failed to load resource at $path")