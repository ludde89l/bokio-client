package se.veritate.bokio.client

import arrow.core.nonEmptyListOf
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.post
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import io.kotest.assertions.json.shouldEqualJson
import io.kotest.core.spec.style.FunSpec
import io.kotest.extensions.wiremock.WireMockListener
import io.kotest.inspectors.forSingle
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe
import kotlinx.datetime.DatePeriod
import kotlinx.datetime.LocalDate
import kotlinx.datetime.plus
import se.veritate.bokio.client.BokioClient
import se.veritate.bokio.client.CompanyId
import se.veritate.bokio.client.Credentials
import se.veritate.bokio.client.Customer
import se.veritate.bokio.client.CustomerId
import se.veritate.bokio.client.InvoiceId
import se.veritate.bokio.client.InvoiceRow
import se.veritate.bokio.client.VatPercentage

class CreateInvoiceTest : FunSpec(
   {

      data class CompanyData(
         val name: String,
         val uuid: String,
      )

      // Must match /responses/bokio/user.json
      val myCompany = CompanyData("My Company Name AB", "29146d71-23c6-4112-82ca-f97caee9863e")

      data class CustomerData(
         val name: String,
         val uuid: String,
      )

      val sampleCustomer = CustomerData("Boneo AB", "9357809b-4b08-4193-bc9d-8ee2a8a2916a")

      val mockBokio = wireMockServer { dynamicPort() }
      extensions(WireMockListener.perSpec(mockBokio))

      context("Creating an invoice") {
         mockBokio.stubbing(
            post("/Account/Login") returnsJsonResource "/responses/bokio/login.json",
            get("/Settings/User") returnsJsonResource "/responses/bokio/user.json",
            post("/${myCompany.uuid}/Invoices/Invoice/CreateInvoice") returnsJsonResource "/responses/bokio/create_invoice.json",
            post("/${myCompany.uuid}/Invoices/Invoice/PublishInvoice") returnsJsonResource "/responses/bokio/publish_invoice.json",
            post("/${myCompany.uuid}/Invoices/Invoice/SetCustomer") returnsJsonResource "/responses/bokio/set_customer.json",
            post("/${myCompany.uuid}/Invoices/Invoice/AddRow") returnsJsonResource "/responses/bokio/add_row.json",
            get("/${myCompany.uuid}/Invoices/Customer/ListBasicDetails") returnsJsonResource "/responses/bokio/customers.json",
            get("/${myCompany.uuid}/Invoices/Customer/Get/${sampleCustomer.uuid}") returnsJsonResource "/responses/bokio/customer.json",
         )

         val client = BokioClient.forCompany(
            myCompany.name,
            Credentials("username", "password"),
            baseUrl = "http://localhost:${mockBokio.port()}",
         )

         val invoiceDate = LocalDate.parse("2021-01-01")
         val dueDate = invoiceDate.plus(DatePeriod(days = 100))

         var notifiedCustomer: Customer? = null
         var notifiedCompany: CompanyId? = null
         var notifiedInvoiceId: InvoiceId? = null

         client.createInvoice(
            sampleCustomer.name,
            nonEmptyListOf(InvoiceRow("Some service", 1.0, 100.0, InvoiceRow.RowKind.SERVICES, VatPercentage.TWENTY_FIVE)),
            invoiceDate = invoiceDate,
            paymentDays = 100u,
            autoPublish = true,
            notificationStrategy = { customer, company, invoiceId, _ ->
               notifiedCustomer = customer
               notifiedCompany = company
               notifiedInvoiceId = invoiceId
            },
         ).isRight().shouldBeTrue()

         test("Invokes notification strategy with correct data") {
            notifiedCompany shouldBe CompanyId(myCompany.uuid)
            notifiedCustomer shouldBe Customer(CustomerId(sampleCustomer.uuid), "Boneo AB", null)
            notifiedInvoiceId shouldBe InvoiceId("35cca191-9a21-4cb2-a75d-642a92b80ec6")
         }

         test("Sends correct requests to Bokio") {
            mockBokio
               .findRequestsMatching(postRequestedFor(urlPathEqualTo("/${myCompany.uuid}/Invoices/Invoice/CreateInvoice")).build())
               .requests.forSingle {
                  it.bodyAsString shouldEqualJson """
                     {
                       "Deadline": "$dueDate",
                       "InvoiceDate": "$invoiceDate",
                       "InvoiceFee": 0,
                       "RecurringInvoiceEnabled": false,
                       "Type": "Invoice"
                     }
                  """.trimIndent()
               }

            mockBokio
               .findRequestsMatching(postRequestedFor(urlPathEqualTo("/${myCompany.uuid}/Invoices/Invoice/SetCustomer")).build())
               .requests.forSingle {
                  it.bodyAsString shouldEqualJson """
                     {
                        "CustomerId": "${sampleCustomer.uuid}",
                        "InvoiceId": "35cca191-9a21-4cb2-a75d-642a92b80ec6"
                     }
                  """.trimIndent()
               }

            mockBokio
               .findRequestsMatching(postRequestedFor(urlPathEqualTo("/${myCompany.uuid}/Invoices/Invoice/PublishInvoice")).build())
               .requests.forSingle {
                  it.bodyAsString shouldEqualJson """
                     {
                        "InvoiceId": "35cca191-9a21-4cb2-a75d-642a92b80ec6",
                        "InvoiceSystem": "Bokio",
                        "PersonalMessage": "Message"
                     }
                  """.trimIndent()
               }
         }
      }
   },
)
