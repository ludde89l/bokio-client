package se.veritate.bokio.client

external val process: Process

external interface Process {
   val env: dynamic
}

/**
 * Will only work for NodeJS runtime, not browser
 */
actual fun getEnv(variable: String): String = process.env[variable] as? String ?: error("Missing environment variable $variable")