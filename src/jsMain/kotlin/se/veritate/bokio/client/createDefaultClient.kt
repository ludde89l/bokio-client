package se.veritate.bokio.client

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.js.Js
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.cookies.HttpCookies
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json

actual fun createDefaultClient(config: HttpClientConfig<*>.() -> Unit): HttpClient =
   HttpClient(Js) {
      install(ContentNegotiation) {
         json(
            Json {
               ignoreUnknownKeys = true
               encodeDefaults = true
            },
         )
      }

      config()

      install(HttpCookies)
   }
