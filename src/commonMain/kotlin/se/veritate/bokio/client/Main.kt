package se.veritate.bokio.client

import arrow.core.nonEmptyListOf

suspend fun main() {
   val client = BokioClient.forCompany(
      getEnv("BOKIO_COMPANY_NAME"),
      Credentials(getEnv("BOKIO_USERNAME"), getEnv("BOKIO_PASSWORD")),
   )

   client.createInvoice(
      getEnv("BOKIO_CUSTOMER_NAME"),
      nonEmptyListOf(InvoiceRow("Test", 1.0, 1337.0, InvoiceRow.RowKind.SERVICES, VatPercentage.TWENTY_FIVE)),
      notificationStrategy = { customer, _, invoiceId, _ ->
         if (customer.email != null) {
            client.emailInvoice(invoiceId, customer.email, "ASDFG")
         } else {
            println("[warn] No email for customer ${customer.name}, unable to send $invoiceId")
         }
      },
   )
}

expect fun getEnv(variable: String): String
