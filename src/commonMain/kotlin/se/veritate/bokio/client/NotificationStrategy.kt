package se.veritate.bokio.client

typealias NotificationStrategy = suspend (
   customer: Customer,
   companyId: CompanyId,
   invoiceId: InvoiceId,
   invoiceNumber: InvoiceNumber
) -> Unit

val noopNotificationStrategy: NotificationStrategy = { _, _, _, _ -> }