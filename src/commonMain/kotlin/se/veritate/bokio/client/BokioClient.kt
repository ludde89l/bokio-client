package se.veritate.bokio.client

import arrow.core.Either
import arrow.core.NonEmptyList
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.contentType
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import se.veritate.bokio.client.impl.BokioClientImpl
import se.veritate.bokio.client.impl.User

interface BokioClient {
   /**
    * Creates and publishes an invoice.
    * [customerName] must exactly match a customer in Bokio.
    * [paymentDays] is how many days from [invoiceDate] the invoice is due
    */
   suspend fun createInvoice(
      customerName: String,
      rows: NonEmptyList<InvoiceRow>,
      invoiceDate: LocalDate = Clock.System.now().toLocalDateTime(TimeZone.UTC).date,
      paymentDays: UInt = 30u,
      autoPublish: Boolean = false,
      notificationStrategy: NotificationStrategy = noopNotificationStrategy,
   ): Either<CreateInvoiceError, InvoiceId>

   suspend fun publishInvoice(invoiceId: InvoiceId): InvoiceNumber

   suspend fun downloadInvoice(invoiceId: InvoiceId): ByteArray

   suspend fun emailInvoice(invoiceId: InvoiceId, recipient: String, message: String)

   suspend fun registerSupplierInvoice(
      pdfBytes: ByteArray,
      supplierName: String,
      invoiceDate: LocalDate,
      dueDate: LocalDate,
      title: String,
      transactions: List<Transaction>,
   )

   suspend fun fetchCustomer(customerId: CustomerId): Customer

   companion object {
      suspend fun forCompany(
         companyName: String,
         credentials: Credentials,
         baseUrl: String = "https://app.bokio.se",
         clientConfig: HttpClient.() -> Unit = {},
      ): BokioClient {
         suspend fun HttpClient.findCompanyByName(companyName: String): CompanyId {
            val user = get("/Settings/User").body<User>()
            return user.CompanyMemberships.singleOrNull { it.CompanyName == companyName }?.CompanyId
               ?: error("Company $companyName not found (or not accessible with the given credentials) found: " +
                  user.CompanyMemberships.map { it.CompanyName }.joinToString(", ")
               )
         }

         val client = createDefaultClient {
            defaultRequest {
               url(baseUrl)
            }
         }.apply(clientConfig)

         client.post("/Account/Login") {
            contentType(ContentType.Application.Json)
            setBody(credentials)
         }

         return BokioClientImpl(
            client,
            client.findCompanyByName(companyName),
         )
      }
   }
}
