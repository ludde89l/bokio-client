package se.veritate.bokio.client

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.jvm.JvmInline

@Serializable
data class Credentials(
   val username: String,
   val password: String,
)

@JvmInline
@Serializable
value class CompanyId(val id: String) {
   override fun toString(): String {
      return id
   }
}

@JvmInline
@Serializable
value class InvoiceId(val id: String)

@JvmInline
@Serializable
value class CustomerId(val id: String) {
   override fun toString(): String {
      return id
   }
}

data class Transaction(
   val amount: Long,
   val account: Long,
)

/**
 * Human-readable identifier of the invoice, typically a number but might include a text prefix
 */
@JvmInline
value class InvoiceNumber(val number: String)

data class Customer(
   val id: CustomerId,
   val name: String,
   val email: String?,
)

data class InvoiceRow(
   val description: String,
   val quantity: Double,
   val price: Double,
   val kind: RowKind,
   val vatPercentage: VatPercentage,
   val unitType: UnitType = UnitType.HOUR,
) {

   @Serializable
   enum class RowKind {
      @SerialName("Services")
      SERVICES,

      @SerialName("Goods")
      GOODS,
   }

   @Serializable
   enum class UnitType {
      @SerialName("Hour")
      HOUR,

      @SerialName("Day")
      DAY,

      @SerialName("Piece")
      PIECE,
   }
}

enum class VatPercentage {
   ZERO,
   SIX,
   TWELVE,
   TWENTY_FIVE,
   ;

   fun toDouble() = when (this) {
      ZERO -> 0.0
      SIX -> 0.06
      TWELVE -> 0.12
      TWENTY_FIVE -> 0.25
   }
}

sealed interface CreateInvoiceError {
   data class CustomerNotFound(val customerName: String) : CreateInvoiceError
}
