package se.veritate.bokio.client

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig

expect fun createDefaultClient(config: HttpClientConfig<*>.() -> Unit): HttpClient