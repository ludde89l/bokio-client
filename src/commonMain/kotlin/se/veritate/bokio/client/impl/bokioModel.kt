package se.veritate.bokio.client.impl

import kotlinx.datetime.LocalDate
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import se.veritate.bokio.client.CompanyId
import se.veritate.bokio.client.CustomerId
import se.veritate.bokio.client.InvoiceId
import se.veritate.bokio.client.InvoiceRow.RowKind
import se.veritate.bokio.client.InvoiceRow.UnitType
import kotlin.jvm.JvmInline

@Serializable
internal data class Data<T>(
   val Data: T,
)

@Serializable
internal data class PublishInvoiceResponse(
   val InvoiceNumberWithPrefix: String,
)

@Serializable
internal data class UploadFileResponse(
   val Id: FileId,
)

@JvmInline
@Serializable
value class SupplierId(val id: String)

@JvmInline
@Serializable
value class FileId(val id: String)

@JvmInline
@Serializable
value class SupplierInvoiceId(val id: String)

@Serializable
data class CreateSupplierInvoiceRequest(
   val InvoiceTitle: String,
   val Date: LocalDateAsString,
   val Deadline: LocalDateAsString,
   val Sum: Long,
   val SupplierId: SupplierId,
   val ReceiptId: FileId,
) {
   val Type = "Invoice"
   val PaymentMessage = ""
   val PaymentMessageType = ""
}

@Serializable
data class CreateSupplierInvoiceResponse(
   @SerialName("Data")
   val Id: SupplierInvoiceId,
)

@JvmInline
@Serializable
internal value class UserId(val id: String)

@Serializable
internal data class User(
   val Email: String,
   val FirstName: String,
   val LastName: String,
   val PhoneNumber: String?,
   val Language: String,
   val EmailConfirmed: Boolean,
   val UserResearchGroup: Boolean,
   val Languages: List<Language>,
   @SerialName("Id") val id: UserId,
   val CompanyMemberships: List<CompanyMemberships>,
   val UserFeatureToggles: List<UserFeatureToggles>,
)

@Serializable
internal data class Language(
   val Name: String,
   val DisplayName: String,
   val EnglishName: String,
)

@Serializable
internal data class CompanyMemberships(
   val CompanyName: String,
   val Type: String,
   val CompanyId: CompanyId,
   val OrgNumber: String,
   val OtherMembers: Boolean,
   val IsOwner: Boolean,
)

@Serializable
internal data class UserFeatureToggles(
   val FeatureKey: String,
   val Enabled: Boolean,
)

@Serializable
internal data class CreateInvoiceRequest(
   @SerialName("Deadline")
   val dueDate: LocalDate,
   @SerialName("InvoiceDate")
   val invoiceDate: LocalDateAsString,
   val InvoiceFee: Int = 0,
   val RecurringInvoiceEnabled: Boolean = false,
   val Type: String = "Invoice",
)

@Serializable
internal data class SetInvoiceCustomerRequest(val InvoiceId: InvoiceId, val CustomerId: CustomerId)

@Serializable
internal data class BasicCustomer(
   val Id: CustomerId,
   val Name: String,
)

@Serializable
internal data class CustomerDto(
   val Id: CustomerId,
   val DefaultInvoiceReciverData: Receiver,
)

@Serializable
internal data class SupplierDto(
   val SupplierName: String,
   val SupplierId: SupplierId,
)

@Serializable
internal data class Receiver(
   val Email: String?,
   val Name: String,
)

@Serializable
internal data class CreateInvoiceResponse(
   @SerialName("Id") val id: InvoiceId,
)

internal data class PublishSupplierInvoiceAccountingRequest(
   val Title: String,
   val SupplierInvoiceId: SupplierInvoiceId,
   val RecieptId: FileId,
   val Date: LocalDateAsString,
   val ExpectPaymentAt: LocalDateAsString,
   val Transactions: List<TransactionDto>,
   val IgnoreDuplicateCheck: Boolean = false,
)

@Serializable
internal data class TransactionDto(
   val Account: Long,
   val AccountName: String = "",
   val Credit: Long,
   val Debet: Long,
   val Id: Long = 0,
)

/**
 * Example:
 * ```json
 * {
 *     "Index": 0,
 *     "Description": "Konsultation",
 *     "Units": 1,
 *     "UnitPrice": 1000,
 *     "UnitType": "Hour",
 *     "VAT": 0.25,
 *     "ProductCode": "",
 *     "AccountNbr": 0,
 *     "Kind": "Services",
 *     "IsTextRow": false,
 *     "TemplateName": "",
 *     "InvoiceId": "098310d6-99af-424f-ab7a-3c49fe71e5d2"
 * }
 * ```
 */
@Serializable
internal data class AddInvoiceRowRequest(
   val InvoiceId: InvoiceId,
   val Description: String,
   val Units: Double,
   val UnitPrice: Double,
   val IsTextRow: Boolean = false,
   val Kind: RowKind = RowKind.SERVICES,
   val ProductCode: String = "",
   val TemplateName: String = "",
   val UnitType: UnitType,
   val VAT: Double = 0.25,
   val AccountNbr: Int = 0,
   val Index: Int = 0,
)

@Serializable
internal data class PublishInvoiceRequest(
   val InvoiceId: InvoiceId,
   val InvoiceSystem: String,
   val PersonalMessage: String,
)

@Serializable
internal data class SendEmailRequest(
   val DataJson: String,
   val Email: String,
   val InvoiceId: InvoiceId,
   val Method: String,
   val PersonalMessage: String,
)
