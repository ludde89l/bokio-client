package se.veritate.bokio.client.impl

import arrow.core.Either
import arrow.core.Nel
import arrow.core.raise.either
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.forms.formData
import io.ktor.client.request.forms.submitFormWithBinaryData
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.Headers
import io.ktor.http.HttpHeaders
import io.ktor.http.contentType
import kotlinx.datetime.DatePeriod
import kotlinx.datetime.LocalDate
import kotlinx.datetime.plus
import se.veritate.bokio.client.BokioClient
import se.veritate.bokio.client.CompanyId
import se.veritate.bokio.client.CreateInvoiceError
import se.veritate.bokio.client.Customer
import se.veritate.bokio.client.CustomerId
import se.veritate.bokio.client.InvoiceId
import se.veritate.bokio.client.InvoiceNumber
import se.veritate.bokio.client.InvoiceRow
import se.veritate.bokio.client.NotificationStrategy
import se.veritate.bokio.client.Transaction
import se.veritate.bokio.client.noopNotificationStrategy

class BokioClientImpl(
   private val client: HttpClient,
   private val companyId: CompanyId,
) : BokioClient {
   override suspend fun createInvoice(
      customerName: String,
      rows: Nel<InvoiceRow>,
      invoiceDate: LocalDate,
      paymentDays: UInt,
      autoPublish: Boolean,
      notificationStrategy: NotificationStrategy,
   ): Either<CreateInvoiceError, InvoiceId> = either {

      val customerId = findCustomerByName(customerName) ?: raise(CreateInvoiceError.CustomerNotFound(customerName))

      val invoiceId = client.post("/${companyId.id}/Invoices/Invoice/CreateInvoice") {
         contentType(ContentType.Application.Json)
         setBody(
            CreateInvoiceRequest(
               invoiceDate = invoiceDate,
               dueDate = invoiceDate.plus(DatePeriod(days = paymentDays.toInt())),
            ),
         )
      }.body<CreateInvoiceResponse>().id
      
      setInvoiceCustomer(invoiceId, customerId)

      rows.forEach { row -> addInvoiceRow(invoiceId, row) }

      if (autoPublish) {
         val invoiceNumber = publishInvoice(invoiceId)
         notificationStrategy.invoke(fetchCustomer(customerId), companyId, invoiceId, invoiceNumber)
      } else {
         if (notificationStrategy != noopNotificationStrategy) {
            println("WARNING: Notification strategy was set, but invoice is not published")
         }
      }

      invoiceId
   }

   private suspend fun addInvoiceRow(invoiceId: InvoiceId, row: InvoiceRow) {
      client.post("/${companyId.id}/Invoices/Invoice/AddRow") {
         contentType(ContentType.Application.Json)
         setBody(
            AddInvoiceRowRequest(
               InvoiceId = invoiceId,
               Description = row.description,
               Units = row.quantity,
               UnitPrice = row.price,
               Kind = row.kind,
               VAT = row.vatPercentage.toDouble(),
               UnitType = row.unitType,
            ),
         )
      }
   }

   override suspend fun publishInvoice(invoiceId: InvoiceId): InvoiceNumber =
      client.post("/${companyId.id}/Invoices/Invoice/PublishInvoice") {
         contentType(ContentType.Application.Json)
         setBody(
            PublishInvoiceRequest(
               InvoiceId = invoiceId,
               InvoiceSystem = "Bokio",
               PersonalMessage = "Message",
            ),
         )
      }.body<Data<PublishInvoiceResponse>>()
         .Data
         .InvoiceNumberWithPrefix
         .let(::InvoiceNumber)

   override suspend fun downloadInvoice(invoiceId: InvoiceId): ByteArray =
      client.get("/${companyId.id}/Invoices/Invoice/LatestPdf/${invoiceId.id}") {
         contentType(ContentType.Application.Json)
      }.body()

   override suspend fun emailInvoice(invoiceId: InvoiceId, recipient: String, message: String) {
      client.post("/${companyId.id}/Invoices/Invoice/SendPdf") {
         contentType(ContentType.Application.Json)
         setBody(
            SendEmailRequest(
               InvoiceId = invoiceId,
               DataJson = "{\"DeliveryType\":\"LinkAndPdf\",\"Recipient\":\"${recipient}\"}",
               Email = recipient,
               Method = "Email",
               PersonalMessage = message,
            ),
         )
      }
   }

   override suspend fun registerSupplierInvoice(
      pdfBytes: ByteArray,
      supplierName: String,
      invoiceDate: LocalDate,
      dueDate: LocalDate,
      title: String,
      transactions: List<Transaction>,
   ) {
      val fileId = uploadFile(pdfBytes)

      val supplierInvoiceId = createSupplierInvoice(
         supplierName,
         fileId,
         invoiceDate,
         dueDate,
         transactions.find { it.account == 2440L }!!.amount,
      )

      publishAccounting(
         PublishSupplierInvoiceAccountingRequest(
            Title = title,
            SupplierInvoiceId = supplierInvoiceId,
            RecieptId = fileId,
            Transactions = transactions.map { it.toDto() },
            Date = invoiceDate,
            ExpectPaymentAt = dueDate,
         ),
      )
   }

   private suspend fun publishAccounting(request: PublishSupplierInvoiceAccountingRequest) {
      client.post("/${companyId.id}/Accounting/Verification/RegisterSupplierInvoice") {
         contentType(ContentType.Application.Json)
         setBody(request)
      }
   }

   private fun Transaction.toDto(): TransactionDto = TransactionDto(
      Account = account,
      Credit = if (amount < 0) -amount else 0,
      Debet = if (amount >= 0) amount else 0,
   )

   private suspend fun createSupplierInvoice(
      supplierName: String,
      fileId: FileId,
      invoiceDate: LocalDate,
      dueDate: LocalDate,
      totalAmount: Long,
   ): SupplierInvoiceId {
      val supplierId = findSupplierByName(supplierName) ?: error("Unable to find supplier $supplierName")
      return client.post("/${companyId.id}/Accounting/SupplierInvoice/Add") {
         contentType(ContentType.Application.Json)
         setBody(
            CreateSupplierInvoiceRequest(
               SupplierId = supplierId,
               ReceiptId = fileId,
               Date = invoiceDate,
               Deadline = dueDate,
               Sum = totalAmount,
               InvoiceTitle = supplierName,
            ),
         )
      }.body<CreateSupplierInvoiceResponse>().Id
   }

   private suspend fun findSupplierByName(name: String): SupplierId? =
      getSuppliers()
         .find { it.SupplierName == name }
         ?.SupplierId

   private suspend fun getSuppliers(): List<SupplierDto> =
      client
         .get("/${companyId.id}/Accounting/Suppliers/GetStatusOfSuppliers")
         .body<Data<List<SupplierDto>>>()
         .Data

   private suspend fun uploadFile(file: ByteArray): FileId =
      client.submitFormWithBinaryData(
         url = "/${companyId.id}/Accounting/Receipt/UploadPdf", // UploadReceiptWithFile if not PDF
         formData = formData {
            append(
               "file",
               file,
               Headers.build {
                  append("Content-Type", "application/octet-stream")
                  append(HttpHeaders.ContentDisposition, "form-data; name=\"file\"; filename=\"invoice.pdf\"")
               },
            )
            append("selectedType", "supplierInvoice")
            append("startRecepiptPredictionsNow", "true") // Typoed in Bokio, this might break (be fixed) at some point
         },
      ).body<UploadFileResponse>().Id

   private suspend fun findCustomerByName(name: String): CustomerId? {
      return fetchCustomers().find { it.Name == name }?.Id
   }

   private suspend fun fetchCustomers(): List<BasicCustomer> {
      return client.get("/${companyId.id}/Invoices/Customer/ListBasicDetails") {
         contentType(ContentType.Application.Json)
      }.body()
   }

   override suspend fun fetchCustomer(customerId: CustomerId): Customer {
      return client.get("/$companyId/Invoices/Customer/Get/$customerId") {
         contentType(ContentType.Application.Json)
      }.body<Data<CustomerDto>>().Data.let { dto ->
         Customer(
            dto.Id,
            dto.DefaultInvoiceReciverData.Name,
            dto.DefaultInvoiceReciverData.Email,
         )
      }
   }

   private suspend fun setInvoiceCustomer(invoiceId: InvoiceId, customerId: CustomerId) {
      client.post("/${companyId.id}/Invoices/Invoice/SetCustomer") {
         contentType(ContentType.Application.Json)
         setBody(
            SetInvoiceCustomerRequest(
               InvoiceId = invoiceId,
               CustomerId = customerId,
            ),
         )
      }
   }
}
