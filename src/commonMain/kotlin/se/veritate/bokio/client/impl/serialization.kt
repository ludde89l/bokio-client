package se.veritate.bokio.client.impl

import io.ks3.standard.stringSerializer
import kotlinx.datetime.LocalDate
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable

object KotlinxLocalDateSerializer : KSerializer<LocalDate> by stringSerializer(
   { LocalDate.parse(it) },
   { it.toString() },
)

typealias LocalDateAsString = @Serializable(with = KotlinxLocalDateSerializer::class) LocalDate
