@Suppress("DSL_SCOPE_VIOLATION")
plugins {
   alias(libs.plugins.kotlin.multiplatform)
   alias(libs.plugins.kotlin.pluginSerialization)
   alias(libs.plugins.kotlinx.binaryCompatibilityValidator)
   alias(libs.plugins.testlogger)
   application

   `maven-publish`
   alias(libs.plugins.npmPublish)
   signing
}

group = "se.veritate"
version = "0.1.2-SNAPSHOT"

repositories {
   mavenLocal()
   mavenCentral()

   maven(url = "https://oss.sonatype.org/content/repositories/snapshots") {
      mavenContent { snapshotsOnly() }
   }

   maven(url = "https://s01.oss.sonatype.org/content/repositories/snapshots") {
      mavenContent { snapshotsOnly() }
   }
}

val javadocJarStub by tasks.registering(Jar::class) {
   group = JavaBasePlugin.DOCUMENTATION_GROUP
   description = "Empty Javadoc Jar (required by Maven Central)"
   archiveClassifier.set("javadoc")
}

publishing {
   repositories {
      maven {
         url = uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
         credentials {
            username = properties["repo.username"].toString()
            password = properties["repo.password"].toString()
         }
      }
   }
   publications {
      publications.withType<MavenPublication>().configureEach {

         artifact(javadocJarStub)

         pom {
            name.set(project.name)
            description.set("A client for interacting with Bokios REST API")
            url.set("https://gitlab.com/ludde89l/bokio-client")

            scm {
               connection.set("scm:git:git://gitlab.com/ludde89l/bokio-client.git")
               developerConnection.set("scm:git:ssh://gitlab.com:ludde89l/bokio-client.git")
               url.set("https://gitlab.com/ludde89l/bokio-client")
            }

            licenses {
               license {
                  name.set("Apache License, Version 2.0")
                  url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                  distribution.set("repo")
               }
            }

            developers {
               developer {
                  id.set("Kantis")
                  name.set("Emil Kantis")
               }
               developer {
                  id.set("maglun")
                  name.set("Magnus Lundberg")
               }
            }
         }
      }
   }
}

tasks.withType<Test>().configureEach {
   useJUnitPlatform()
}

kotlin {
   targets {
      js(IR) {
         binaries.library()
         nodejs()
      }

      jvm {
         jvmToolchain(11)
      }
   }

   sourceSets {
      val commonMain by getting {
         dependencies {
            api(libs.arrow.core)
            api(libs.ktorClient.core)
            implementation(libs.ks3.standard)
            implementation(libs.kotlinx.datetime)
            implementation(libs.kotlinx.coroutinesCore)
            implementation(libs.ktorClient.contentNegotiation)
            implementation(libs.slf4j.api)
            implementation(libs.log4j2.api)
            implementation(libs.log4j2.core)
            implementation(libs.log4j2.slf4jImpl)
            implementation(libs.ktorSerialization.kotlinxJson)
         }
      }

      val commonTest by getting {
         dependencies {
            implementation(libs.kotest.assertionsJson)
         }
      }

      val jsMain by getting {
         dependencies {
            implementation(libs.kotlinx.coroutinesCoreJs)
            implementation(libs.ktorClient.engineJs)
         }
      }

      val jvmMain by getting {
         dependsOn(commonMain)
         dependencies {
            implementation(libs.kotest.runnerJunit5)
            implementation(libs.ks3.jdk)
            api(libs.ktorClient.cio)
            implementation(libs.ktorClient.loggingJvm)
            implementation(libs.kotestExtensions.wiremock)
            implementation(libs.wiremock)
         }
      }

      val jvmTest by getting {
         dependsOn(commonTest)
      }
   }
}

application {
   mainClass.set("MainKt")
}

signing {
   sign(publishing.publications)
}

val signingTasks = tasks.withType<Sign>()
tasks.withType<AbstractPublishToMaven>().configureEach {
   dependsOn(signingTasks)
}

npmPublish {
   registries {
      npmjs {
         authToken.set(properties["npm.access.token"].toString())
      }
   }
   organization.set("veritate")
   packages {
      named("js") {
         packageJson {
            "license" by "Apache-2.0"
         }
         version.set("v0.1.1")
      }
   }
}
