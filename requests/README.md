To use the sample requests create a file called `http-client.private.env.json` with the contents:
```json
{
  "bokio": {
    "companyId": "<Your company ID>",
    "username": "<Your email>",
    "password": "<Your password>"
  }
}
```

`login.http` must be run first to get a session, then other requests can be run.